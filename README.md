# install

```shell
yarn add gitlab:yt-lib/fastify-openapi-service#$VERSION
```

# usage example

show "src" directory.
"src/gen" is generated codes.

# usage

```ts
import { promises as fs } from 'fs'
import * as path from 'path'
import yaml from 'yaml'
import { createFastifyOpenapiService } from 'fastify-openapi-service'

const __root = path.join(__dirname, '..')

const openapi = yaml.parse(
	await fs.readFile(path.join(__root, 'openapi.yml'), 'utf-8'),
)
await createFastifyOpenapiService(openapi, path.join(__root, 'src/gen'))
```

# notice

operationId is required.
