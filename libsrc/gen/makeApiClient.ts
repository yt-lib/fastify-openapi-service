import * as ts from 'typescript'

import type { Converter } from '@himenon/openapi-typescript-code-generator'

const { stringify: e } = JSON

export const makeClientApiClient: Converter.v3.Generator.MakeApiClientFunction = (
	context: ts.TransformationContext,
	codeGeneratorParamsList: Converter.v3.CodeGeneratorParams[],
): ts.Statement[] => {
	const statements: ts.Statement[] = []
	const methods = codeGeneratorParamsList.map(
		({ operationId, responseSuccessNames, successResponseContentTypes }) => {
			if (!operationId) throw new Error('operationId is required.')
			const returnType = responseSuccessNames
				.map((m, i) => `${m}[${e(successResponseContentTypes[i])}]`)
				.join(' | ')
			return `${operationId}(...args: Args): Promise<${returnType}>\n`
		},
	)
	const source = ts.createSourceFile(
		'hoge',
		`export interface IService<Args extends unknown[]> { ${methods.join('')} }`,
		ts.ScriptTarget.ES2019,
		false,
		ts.ScriptKind.TS,
	)
	for (const s of source.statements) statements.push(s)
	return statements
}
