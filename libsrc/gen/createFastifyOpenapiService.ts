import { promises as fs } from 'fs'
import * as path from 'path'

import * as dtsgenerator from 'dtsgenerator'
import * as generator from '@himenon/openapi-typescript-code-generator'
import { createInterface } from './createInterface'
import { makeClientApiClient } from './makeApiClient'

export const createFastifyOpenapiService = async (
	openapi: dtsgenerator.JsonSchema,
	// params: generator.Params,
	targetDir: string,
) => {
	await fs.mkdir(targetDir, { recursive: true }).catch(Boolean)
	await fs.writeFile(
		path.join(targetDir, 'openapi.ts'),
		[
			'export const openapi = ' + JSON.stringify(openapi) + ' as const',
			'export default openapi',
		].join('\n') + '\n',
	)
	await fs.writeFile(
		path.join(targetDir, '__openapi.json'),
		JSON.stringify(openapi),
	)
	// const d = await dtsgenerator.default({
	// 	contents: [dtsgenerator.parseSchema(openapi)],
	// })
	const d = generator.generateTypeScriptCode({
		entryPoint: path.join(targetDir, '__openapi.json'),
		option: { makeApiClient: makeClientApiClient },
	})
	await fs.unlink(path.join(targetDir, '__openapi.json'))
	const content = [
		d.replace(/(^|\n)(\s*)(declare |)namespace /giu, '$1$2export namespace '),
	].join('\n')
	await fs.writeFile(path.join(targetDir, 'output.ts'), content)
	const itertext = await createInterface(content)
	await fs.writeFile(path.join(targetDir, 'IService.ts'), itertext)
	await fs.writeFile(
		path.join(targetDir, 'index.ts'),
		[
			"export type { FastifyRequest as Req, FastifyReply as Rep } from 'fastify'",
			"export * from './output'",
			"export { openapi } from './openapi'",
			"export type { IService } from './IService'",
		].join('\n') + '\n',
	)
}
