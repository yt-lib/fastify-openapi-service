import { promises as fs } from 'fs'
import * as path from 'path'
import yaml from 'yaml'
import { createFastifyOpenapiService } from '../libsrc/gen'

const main = async () => {
	const __root = path.join(__dirname, '..')
	const openapi = yaml.parse(
		await fs.readFile(path.join(__root, 'openapi.yml'), 'utf-8'),
	)
	await createFastifyOpenapiService(openapi, path.join(__root, 'src/gen'))
}

main().catch(x => {
	console.error(x)
	process.exit(1)
})
