import Fastify from 'fastify'
import openapiGlue from 'fastify-openapi-glue'
import openapi from './gen/openapi'
import { service } from './service'

const fastify = Fastify({ logger: true })
fastify.register(openapiGlue, {
	specification: openapi,
	service,
	// securityHandlers: path.join(__dirname, '/security'),
	// prefix: 'v1',
	noAdditional: true,
	ajvOptions: {
		formats: {},
		// formats: {
		// 	'custom-format': /\d{2}-\d{4}/,
		// },
	},
})

fastify.setErrorHandler((error, req, reply) => {
	const message = error.statusCode ? error.message : 'internal server error.'
	console.error(error)
	reply.status(error.statusCode || 500).send({ message })
})

export const main = async () => {
	fastify.listen(3000, (x, address) => {
		x ? console.error(x) : console.log(`read on ${address}`)
	})
}

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})
