export type { FastifyRequest as Req, FastifyReply as Rep } from 'fastify'
export * from './output'
export { openapi } from './openapi'
export type { IService } from './IService'
