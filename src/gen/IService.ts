import type { FastifyRequest as Req, FastifyReply as Rep } from 'fastify'
import type { IService as _IService } from './output'
export interface IService extends _IService<[Req, Rep]> {}
